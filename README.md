murloc
===
---

A Python library for Blizzard's Battle.net API

I wrote this simple Python library as a way to refamiliarize myself with the
Battle.net API after not having in used it for several years.

Blizzard's API require authentication. Guides, reference docs, and keys for
API access can be found on the [Battlenet dev site](https://dev.battle.net).


## Python Version
---

This library was developed and tested with Python 3, and may not work
with Python 2.7.

## Usage
---

Currently, this library only supports the Community APIs, which use an API key
for auth.

```python
apikey = "40-pers0n-h0gg3r-r@1d"
```

Here a few generic usage examples.

###### Realm Status

```python
wow = WoW(region="us", apikey=apikey)
realms = wow.realm_status()

for realm in realms:
    print("{}, {}".format(realm["name"], realm["timezone"]))
```

###### Character Data

The Community guild and character APIs support querying with fields, which
allow multiple datasets to be retrieved with one call.

```python
char = wow.character("Malfurion", "Maunka", fields=["titles", "talents", "feed"])
```

###### Guild Members

```python
guild = wow.guild(realm="Malfurion", name="Ziltoidian Overlords", fields=["members"])
```

Getting a list of guild members, then getting each guild character's profile.

```python
realm = "Malfurion"
guild = wow.guild(realm, name="Ziltoidian Overlords", fields=["members"])

for member in guild["members"]:
    name = member["character"]["name"]
    char = wow.character(realm, name=name)
    pprint.pprint(char)
```

###### Quest Data

```python
quest = wow.quest(13146)
pprint.pprint(quest)
```
