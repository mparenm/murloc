"""a shared namespace for the games in the community API

"""

from murloc import battlenet


class WoW:
    """WoW API functions

    All functions in this namespace return a json document.

    Attributes:
        game (str): The game title in lowercase - "wow"

        region (str): The battlenet region name

        locale (str): The language locale

        apikey (str): battlenet apikey to auth the request

    Args:
        apikey (str): The battlenet apikey to be used in the request

        region (str, optional): The name of the requested region

        locale (str, optional): The language locale to be used in the request

    """

    def __init__(self, region=None, locale=None, *args, **kwargs):
        """ """
        self.game = "wow"
        self.region = region
        self.locale = locale
        self.apikey = kwargs.get("apikey") or None

    def _get(self, path, fields=None, *args, **kwargs):
        """calls the battlenet request function, returns the response"""
        _path = ''.join(["/wow", path])
        resp = battlenet.request(path=_path, fields=fields, region=self.region,
                                 locale=self.locale, game=self.game,
                                 apikey=self.apikey)
        return resp

    def guild(self, realm, name, fields=None):
        """returns resources for a guild

        Available fields are: members, achievements, news, and challenge

        Args:
            realm (str): The name of the realm the guild is on

            name (str): The name of the guild

            fields (list, optional): A list of string values to specify which
                                     fields you want returned. If no fields are
                                     specified, only the guild profile is
                                     returned.

        Returns:
            json document

        """
        path = "/".join(["/guild", realm, name])
        return self._get(path=path, fields=fields)

    def character(self, realm, name, fields=None):
        """returns resources for a character

        Available fields are: achievements, appearance, feed, guild, hunter
        pets, items, mounts, pets, pet slots, professions, progression, pvp,
        quests, reputation, statistics, stats, talents, titles, audit.

        Args:
            realm (str): The name of the realm the character is on

            name (str): The name of the character

            fields (list, optional): A list of string values to specify which
                                     fields you want returned. If no fields are
                                     specified, only the chracter profile is
                                     returned.

        Returns:
            json document

        """
        path = "/".join(["/character", realm, name])
        return self._get(path=path, fields=fields)

    def realm_status(self):
        """ """
        path = "/realm/status"
        return self._get(path=path)

    def spell(self, spell_id):
        """ """
        path = "/spell/{}".format(spell_id)
        return self._get(path=path)

    def quest(self, quest_id):
        """ """
        path = "/quest/{}".format(quest_id)
        return self._get(path=path)

    def zone_list(self):
        """ """
        path = "/zone"
        return self._get(path=path)

    def zone(self, zone_id):
        """ """
        path = "/zone/{}".format(_id)
        return self._get(path=path)

    def item(self, item_id):
        """ """
        path = "/item/{}".format(item_id)
        return self._get(path=path)

    def itemset(self, itemset_id):
        """ """
        path = "/item/set/{}".format(itemset_id)
        return self._get(path=path)

    def character_races(self):
        """ """
        path = "/data/character/races"
        return self._get(path=path)

    def character_classes(self):
        """ """
        path = "/data/character/classes"
        return self._get(path=path)

    def battlegroups(self):
        """ """
        path = "/data/battlegroups/"
        return self._get(path=path)

    def leaderboard(self, bracket):
        """pvp leaderboard

        Valid brackets are: '2v2', '3v3', '5v5', and 'rbg'.
        """
        path = "/leaderboard/{}".format(bracket)
        return self._get(path=path)

    def achievements(self):
        """ """
        path = "/data/character/achievements"
        return self._get(path=path)

    def recipe(self, recipe):
        """ """
        path = "/recipe/{}".format(recipe)
        return self._get(path=path)

    def guild_rewards(self):
        """ """
        path = "/data/guild/rewards"
        return self._get(path=path)

    def guild_perks(self):
        """ """
        path = "/data/guild/perks"
        return self._get(path=path)

    def guild_achievements(self):
        """ """
        path = "/data/guild/achievements"
        return self._get(path=path)

    def item_classes(self):
        """ """
        path = "/data/item/classes"
        return self._get(path=path)

    def talents(self):
        """ """
        path = "/data/talents"
        return self._get(path=path)

    def pet_types(self):
        """ """
        path = "/data/pet/types"
        return self._get(path=path)

    def achievement(self, achievement_id):
        """ """
        path = "/achievement{}".format(achievement_id)
        return self._get(path=path)

    def auction_data(self, realm):
        """ """
        path = "/auction/data/{}".format(realm)
        return self._get(path=path)

    def boss_list(self):
        """ """
        path = "/boss/"
        return self._get(path=path)

    def boss(self, boss_id):
        """ """
        path = "/boss/{}".format(boss_id)
        return self._get(path=path)

    def realm_leaderboard(self, realm):
        """ """
        path = "/challenge/{}".format(realm)
        return self._get(path=path)

    def region_leaderboard(self):
        """ """
        path = "/challenge/region"
        return self._get(path=path)

    def mount_list(self):
        """ """
        path = "/mount/"
        return self._get(path=path)

    def pet_list(self):
        """ """
        path = "/pet/"
        return self._get(path=path)

    def pet_abilities(self, ability_id=None):
        """ """
        path = "/pet/{}".format(ability_id)
        return self._get(path=path)

    def pet_species(self, species_id):
        """ """
        path = "/pet/species/{}".format(species_id)
        return self._get(path=path)

    def pet_stats(self, species_id):
        """ """
        path = "/pet/stats/{}".format(species_id)
        return self._get(path=path)
