"""battlenet variables and functions

"""

import requests
from requests.utils import requote_uri


# the API_DOMAIN is used for all regions, except for china
API_DOMAIN = "https://{0}.api.battle.net"
CN_DOMAIN = "https://api.battlenet.com.cn"

DEFAULT_REGION = "us"

# battlenet regions and available locales
regions = {
    "us": {
        "name": "US",
        "host": API_DOMAIN.format("us"),
        "locales": ("en_US", "es_MX", "pt_BR")
    },

    "eu": {
        "name": "EU",
        "host": API_DOMAIN.format("eu"),
        "locales": ("en_GB", "es_ES", "fr_FR", "ru_RU", "de_DE",
                    "pt_PT", "it_IT")
    },

    "kr": {
        "name": "Korea",
        "host": API_DOMAIN.format("kr"),
        "locales": ("ko_KR", )
    },

    "tw": {
        "name": "Taiwan",
        "host": API_DOMAIN.format("tw"),
        "locales": ("zh_TW",)
    },

    "sea": {
        "name": "South East Asia",
        "host": API_DOMAIN.format("sea"),
        "locales": ("en_US",)
    },

    "cn": {
        "name": "China",
        "host": CN_DOMAIN,
        "locales": ("zh_CN",)
    }
}


class Region:
    """a data container for a single region

    Retrieves data from the `regions` dict by doing a lookup based on the
    region name. If no region is passed, it uses `DEFAULT_REGION`. If `locale`
    is None, the locale is set to the first element in the `locales` array.

    If the region or locale requested are invalid, the defaults are used.

    Attributes:
        name (str): the region name

        host (str): the formatted hostname

        locales (tuple): available locales for a given region

        locale (str): the selected locale

    Args:
        region (str): region name

        locale (str): requested locale

    """

    def __init__(self, region=None, locale=None, *args, **kwargs):
        """ """
        _region = self._get_region(region)
        self.name = _region["name"]
        self.host = _region["host"]
        self.locales = _region["locales"]
        self.locale = locale

    def _get_region(self, region=None):
        """searches the `regions` dict for the requested region

        Args:
            region (str): region name

        Returns:
            dict

        """
        _region = region or DEFAULT_REGION
        return regions[_region]

    @property
    def locale(self):
        """ """
        return self._locale

    @locale.setter
    def locale(self, loc=None):
        """ """
        if loc and loc in self.locales:
            self._locale = loc
        else:
            self._locale = self.locales[0]


def request(path=None, apikey=None, region=None, locale=None, params=None,
            headers=None, fields=None, game=None, data_only=True,
            *args, **kwargs):
    """

    Sends resource requests to battlenet

    Args:
        path (str): the resource URI

        apikey (str): battlenet apikey to authorize the request

        region: (str, optional): region name

        locale (str, optional): locale name

        params (dict, optional): key/value pairs to be used in the querystring

        headers (dict, optional): additional http headers

        fields (list, optional): list of requested fields

        data_only (bool, optional): returns only the json document, if true

    Returns:
        A json document, or the full request object if `data_only` is false

    """
    if not path:
        raise TypeError("The path for the request cannot be None")

    if not apikey:
        raise TypeError("The apikey cannot be None")

    if game == "wow" and region == "sea":
        raise TypeError(("SEA only has endpoints for Sc2 game data and oauth "
                         "profiles"))

    _region = Region(region, locale)

    # full url for the request, url encoded
    url = requote_uri(''.join([_region.host, path]))

    _params = params or {}
    _headers = headers

    # add fields, locale, and apikey to the params
    if fields:
        _params["fields"] = fields

    if _params.get("apikey") is None:
        _params["apikey"] = apikey

    if _params.get("locale") is None:
        _params["locale"] = _region.locale

    _params = urlencode_params(_params)

    resp = requests.get(url=url, params=_params, headers=headers)

    # 50 dkp minus if status code not ok
    if resp.status_code != requests.codes.ok:
        resp.raise_for_status()

    return resp.json() if data_only else resp


def urlencode_params(params):
    """returns a dict of url encoded values

    Args:
        params (dict):

    Returns:
        dict with url encoded values
    """
    encoded = {}
    for k, v in params.items():
        _v = v
        if isinstance(v, list) or isinstance(v, tuple):
            _v = ','.join(v)
        encoded[k] = requote_uri(_v)

    return encoded
