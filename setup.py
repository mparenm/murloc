from setuptools import setup
import pkg_resources
import murloc


pkgs = [
    "murloc"
]

requires = [
    "requests"
]


setup(
    name=murloc.__appname__,
    version=murloc.__version__,
    description=murloc.__desc__,
    long_description=murloc.__longdesc__,
    author="mparenm",
    author_email="mparenm@gmail.com",
    packages=pkgs,
    package_data={"murloc": "murloc"},
    include_package_data=True,
    url="",
    install_requires=requires
)
